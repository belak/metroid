#include "director.hpp"
#include <allegro5/allegro_image.h>
#include <allegro5/allegro_font.h>
#include <allegro5/allegro_ttf.h>
#include <allegro5/allegro_primitives.h>
#include <allegro5/allegro_acodec.h>
#include <allegro5/allegro_audio.h>

#include <stack>
#include <iostream>
#include <algorithm>

using namespace std;

// TODO: constant for conf file name

// Some simple sorting funcs for the display mode
bool mode_lt(ALLEGRO_DISPLAY_MODE one, ALLEGRO_DISPLAY_MODE two) {
	// If it has a smaller width or less pixel depth or less pixels, one < two
	return one.width <= two.width &&
		one.width * one.height <= two.width * two.height;
}

bool mode_eq(ALLEGRO_DISPLAY_MODE one, ALLEGRO_DISPLAY_MODE two) {
	return one.width == two.width && one.height == two.height;
}

namespace Director {
	// Public
	ALLEGRO_DISPLAY *display = NULL;
	ALLEGRO_EVENT_QUEUE *input = NULL;
	ALLEGRO_CONFIG *config = NULL;

	vector<ALLEGRO_DISPLAY_MODE> modes;

	ALLEGRO_DISPLAY_MODE mode;
	float targetFPS = 60.0;
	int monitor = 0;

	// TODO: Load these from config
	float bgmVolume = 1.0;
	float sfxVolume = 1.0;

	bool running = true;
	bool fullscreen = false;

	// Private
	stack<screen_ptr> *screens = NULL;
	ALLEGRO_EVENT_QUEUE *events = NULL;
	ALLEGRO_TIMER *timer = NULL;
	ALLEGRO_PATH *config_path = NULL;

	// TODO: Return success or failure
	void init() {
		if (!al_init()) {
			cout << "Allegro failed to start" << endl;
			running = false;
			return;
		}

		// Load extra modules - this only needs to be done once
		// NOTE: Some of these, we should check the return value
		if (!al_install_keyboard()) {
			cout << "keyboard error" << endl;
			running = false;
			return;
		}

		if (!al_install_mouse()) {
			cout << "mouse error" << endl;
			running = false;
			return;
		}

		if (!al_install_audio()) {
			cout << "audio error" << endl;
			running = false;
			return;
		}

		al_init_image_addon();
		al_init_font_addon();
		al_init_ttf_addon();
		al_init_primitives_addon();
		al_init_acodec_addon();

		if(!(events = al_create_event_queue())) {
			cout << "Failed to create event queue" << endl;
			running = false;
			return;
		}

		if(!(input = al_create_event_queue())) {
			cout << "Failed to create input queue" << endl;
			running = false;
			return;
		}

		config_path = al_get_standard_path(ALLEGRO_USER_SETTINGS_PATH);
		al_set_path_filename(config_path, "settings.ini");

		config = al_load_config_file(al_path_cstr(config_path, ALLEGRO_NATIVE_PATH_SEP));
		if (!config) {
			config = al_create_config();
		}

		al_add_config_section(config, "display");

		const char *monitor_num = NULL;
		if (!(monitor_num = al_get_config_value(config, "display", "monitor"))) {
			al_set_config_value(config, "display", "monitor", "0");
			monitor = 0;
		} else {
			monitor = atoi(monitor_num);
			if (monitor < 0 || monitor > al_get_num_video_adapters() - 1) {
				al_set_config_value(config, "display", "monitor", "0");
				monitor = 0;
			}
		}

		al_set_new_display_adapter(monitor);

		// Get the possible display modes
		for (int i = 0; i < al_get_num_display_modes(); i++) {
			ALLEGRO_DISPLAY_MODE temp;
			al_get_display_mode(i, &temp);

			// We only care about width and height
			temp.format = 0;
			temp.refresh_rate = 0;

			modes.push_back(temp);
		}

		sort(modes.begin(), modes.end(), mode_lt);
		modes.erase(unique(modes.begin(), modes.end(), mode_eq), modes.end());

		ALLEGRO_DISPLAY_MODE default_mode = modes.back();

		// TODO: Better var names
		const char *temp = NULL;
		const char *temp2 = NULL;
		bool reset_to_default_mode = false;
		if (!(temp = al_get_config_value(config, "display", "width")) ||
				!(temp2 = al_get_config_value(config, "display", "height"))) {

			// Missing width or height, so reset them
			reset_to_default_mode = true;
		} else {
			mode.width = atoi(temp);
			mode.height = atoi(temp2);
			if (mode.width <= 0 || mode.width > default_mode.width ||
					mode.height <= 0 || mode.height > default_mode.height) {

				// Mode out of bounds, so reset it
				reset_to_default_mode = true;
			}
		}

		if (reset_to_default_mode) {
			char *temp = NULL;
			asprintf(&temp, "%d", default_mode.width);
			al_set_config_value(config, "display", "width", temp);
			free(temp);

			temp = NULL;
			asprintf(&temp, "%d", default_mode.height);
			al_set_config_value(config, "display", "height", temp);
			free(temp);

			mode.height = default_mode.height;
			mode.width = default_mode.width;
		}

		// Reset the stuff we don't need
		mode.format = 0;
		mode.refresh_rate = 0;

		if (!(temp = al_get_config_value(config, "display", "fullscreen"))) {
			fullscreen = true;
			al_set_config_value(config, "display", "fullscreen", "1");
		} else {
			// parse true/false
			if (temp[0] == '1' && temp[1] == '\0') {
				fullscreen = true;
			}
		}

		al_set_path_filename(config_path, "");

		// cout << al_path_cstr(config_path, ALLEGRO_NATIVE_PATH_SEP) << endl;

		reset();

		if(!(timer = al_create_timer(1.0f / targetFPS))) {
			cout << "Failed to create allegro timer" << endl;
			running = false;
			return;
		}

		// Register input locations
		al_register_event_source(events, al_get_display_event_source(display));
		al_register_event_source(events, al_get_timer_event_source(timer));

		al_register_event_source(input, al_get_keyboard_event_source());
		al_register_event_source(input, al_get_mouse_event_source());

		al_start_timer(timer);

		screens = new stack<screen_ptr>();
	}

	void reset() {
		if (display) {
			al_destroy_display(display);
		}

		al_set_new_display_adapter(monitor);

		if (fullscreen) {
			al_set_new_display_flags(ALLEGRO_FULLSCREEN);
		} else {
			al_set_new_display_flags(0);
		}

		// Make sure to load display before everything else
		if (!(display = al_create_display(mode.width, mode.height))) {
			cout << "Failed to create allegro display" << endl;
			running = false;
			return;
		}

		// Reregister the screen for the close event
		al_register_event_source(events, al_get_display_event_source(display));
	}

	void run() {
		while (running) {
			ALLEGRO_EVENT ev;
			al_wait_for_event(events, &ev);

			if (ev.type == ALLEGRO_EVENT_DISPLAY_CLOSE) {
				running = false;
				return;
			} else {
				screens->top()->update(ev);
			}

			// We check if the event queue is empty so we don't try to render more times than we can handle
			if (running && al_is_event_queue_empty(events)) {
				al_clear_to_color(al_map_rgb(0, 0, 0));
				screens->top()->render();

				al_flip_display();
			}
		}
	}

	void cleanup() {
		while (!screens->empty()) {
			screens->pop();
		}

		// TODO: Make sure settings are written to file

		// Do our best to save the conf file
		if (al_make_directory(al_path_cstr(config_path, ALLEGRO_NATIVE_PATH_SEP))) {
			al_set_path_filename(config_path, "settings.ini");
			if (!al_save_config_file(al_path_cstr(config_path, ALLEGRO_NATIVE_PATH_SEP), config)) {
				cout << "config file failed to save" << endl;
			}
			al_set_path_filename(config_path, "");
		} else { 
			cout << "could not create dir for config" << endl;
		}

		al_destroy_path(config_path);
		config_path = NULL;
	}

	void pushScreen(Screen *s) {
		screens->push(screen_ptr(s));
	}

	screen_ptr popScreen() {
		if (screens->empty()) {
			screen_ptr temp = shared_ptr<Screen>(nullptr);
			return temp;
		}

		screen_ptr temp = screens->top();
		screens->pop();

		return temp;
	}

	screen_ptr peekScreen() {
		return screens->top();
	}

	screen_ptr replaceScreen(Screen *s) {
		screen_ptr temp = popScreen();
		pushScreen(s);
		return temp;
	}
}
