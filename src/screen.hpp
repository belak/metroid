#pragma once

#include <allegro5/allegro.h>
#include <memory>

class Screen {
public:
	Screen() {}
	virtual ~Screen() {};
	virtual void update(ALLEGRO_EVENT &ev) {};
	virtual void render() {};
};

typedef std::shared_ptr<Screen> screen_ptr;