#pragma once

#include "screen.hpp"
#include <allegro5/allegro.h>

#include <vector>

namespace Director {
	// Load the gamespace
	void init();
	
	// Run the main loop
	void run();

	// Clean up the gamespace
	void cleanup();

	// Resets monitor
	void reset();

	void pushScreen(Screen *s);
	screen_ptr popScreen();
	screen_ptr replaceScreen(Screen *s);

	extern ALLEGRO_DISPLAY *display;
	extern ALLEGRO_EVENT_QUEUE *input;
	extern ALLEGRO_CONFIG *config;
	extern std::vector<ALLEGRO_DISPLAY_MODE> modes;

	extern ALLEGRO_DISPLAY_MODE mode;
	extern float targetFPS;
	extern bool fullscreen;
	extern int monitor;
	extern bool running;

	extern float bgmVolume;
	extern float sfxVolume;
};
