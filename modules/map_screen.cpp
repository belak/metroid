#include "map_screen.hpp"
#include "../src/director.hpp"

#include <iostream>

using namespace std;

MapScreen::MapScreen(std::string filename) :
		Screen(), world(b2Vec2(0.0f, -9.8f)),  map() {
	cout << "map screen created" << endl;
	this->timer = 0.0f;
	map.ParseFile(filename);

	// Load the tileset's graphics
	for (int i = 0; i < map.GetNumTilesets(); ++i) {
		const Tmx::Tileset *tileset = map.GetTileset(i);
		string filename = "assets/maps/" + tileset->GetImage()->GetSource();
		ALLEGRO_BITMAP *tilesetImg = al_load_bitmap(filename.c_str());
		tilesets.push_back(tilesetImg);

		// Create sub-bitmaps for each tile in the tileset
		int tilesInRow = (tileset->GetImage()->GetWidth() - 2 * tileset->GetMargin() + tileset->GetSpacing()) / (tileset->GetTileWidth() + tileset->GetSpacing());
		int tilesInCol = (tileset->GetImage()->GetHeight() - 2 * tileset->GetMargin() + tileset->GetSpacing()) / (tileset->GetTileHeight() + tileset->GetSpacing());

		// Create a sub-bitmap for each tile
		for (int x = 0; x < tilesInRow; ++x) {
			for (int y = 0; y < tilesInCol; ++y) {
				// The starting x and y for each tile
				int px = tileset->GetMargin() + x * (tileset->GetSpacing() + tileset->GetTileWidth());
				int py = tileset->GetMargin() + y * (tileset->GetSpacing() + tileset->GetTileHeight());

				// Create the sub-bitmap
				ALLEGRO_BITMAP *tile = al_create_sub_bitmap(tilesetImg, px, py, tileset->GetTileWidth(), tileset->GetTileHeight());
				
				// Add the bitmap to the tiles map
				int id = y * tilesInRow + x + tileset->GetFirstGid();
				tiles[id] = tile;
			}
		}
	}

	for (auto layer : map.GetLayers()) {
		auto map_layer = make_shared<MapLayer>();

		map_layer->visible = layer->IsVisible();
		map_layer->height = layer->GetHeight();
		map_layer->width = layer->GetWidth();

		for (int x = 0; x < layer->GetWidth(); x++) {
			for (int y = 0; y < layer->GetHeight(); y++) {
				auto tile = layer->GetTile(x, y);

				if (tile.tilesetId >= 0) {
					auto map_tile = make_shared<MapTile>();
					auto tileset = map.GetTileset(tile.tilesetId);

					if (layer->IsTileFlippedHorizontally(x, y)) {
						map_tile->flags |= ALLEGRO_FLIP_HORIZONTAL;
					}

					if (layer->IsTileFlippedVertically(x, y)) {
						map_tile->flags |= ALLEGRO_FLIP_VERTICAL;
					}

					if (layer->IsTileFlippedDiagonally(x, y)){
						map_tile->flags ^= ALLEGRO_FLIP_HORIZONTAL;
						map_tile->angle = ALLEGRO_PI / 2.0;
					}

					int tileId = tile.id + tileset->GetFirstGid();
					map_tile->tile = tiles[tileId];

					if (map_tile->tile != NULL) {
						map_tile->width = 1.0;
						map_tile->height = 1.0;
						map_tile->loc_x = x;
						map_tile->loc_y = y;

						//cout << map_tile->width << " " << map_tile->height << " " << map_tile->loc_x << " " << map_tile->loc_y << endl;

						map_layer->tiles.push_back(map_tile);
					}
				}
			}
		}

		layers.push_back(map_layer);
	}

	for (auto object_group : map.GetObjectGroups()) {
		auto map_object_group = make_shared<MapObjectGroup>();
		for (auto object : object_group->GetObjects()) {
			auto ellipse = object->GetEllipse();
			auto polygon = object->GetPolygon();
			auto polyline = object->GetPolyline();

			if (ellipse != nullptr) {
				auto map_ellipse = shared_ptr<MapObject>(new MapEllipse(object, world));
				map_object_group->objects.push_back(map_ellipse);
			} else if (polygon != nullptr) {
				auto map_polygon = shared_ptr<MapObject>(new MapPolygon(object, world));
				map_object_group->objects.push_back(map_polygon);
			} else if (polyline != nullptr) {
				auto map_polyline = shared_ptr<MapObject>(new MapPolyline(object, world));
				map_object_group->objects.push_back(map_polyline);
			} else {
				auto map_rectangle = shared_ptr<MapObject>(new MapRectangle(object, world));
				map_object_group->objects.push_back(map_rectangle);
			}
		}

		object_groups.push_back(map_object_group);
	}



	tiles_to_draw_x = Director::mode.width / map.GetTileWidth();
	tiles_to_draw_y = Director::mode.height / map.GetTileHeight();
}

MapScreen::~MapScreen() {
	cout << "map screen destroy" << endl;

	// Free the sub-bitmaps
	// NOTE: This has to be done before the tilesets are freed or bad things will happen
	for (std::pair<int, ALLEGRO_BITMAP *> temp : tiles) {
		ALLEGRO_BITMAP *tileset = temp.second;
		al_destroy_bitmap(tileset);
	}
	tiles.clear();

	// Free the tilesets
	while (!tilesets.empty()) {
		ALLEGRO_BITMAP *tileset = tilesets.back();
		tilesets.pop_back();
		al_destroy_bitmap(tileset);
	}
}

void MapScreen::update(ALLEGRO_EVENT &ev) {
	//int start_x = offset_x / map.GetTileWidth();
	//int start_y = offset_y / map.GetTileHeight();
	//int end_x = start_x + tiles_to_draw_x;
	//int end_y = start_y + tiles_to_draw_y;

	// On each tick, we run through all the input
	while (!al_is_event_queue_empty(Director::input)) {
		ALLEGRO_EVENT iev;
		al_wait_for_event(Director::input, &iev);
		if (iev.type == ALLEGRO_EVENT_KEY_DOWN) {
			switch (iev.keyboard.keycode) {
			case ALLEGRO_KEY_RIGHT:
				break;
			case ALLEGRO_KEY_LEFT:
				break;
			case ALLEGRO_KEY_DOWN:
				break;
			case ALLEGRO_KEY_UP:
				break;
			}
		} else if (iev.type == ALLEGRO_EVENT_KEY_UP) {
			switch (iev.keyboard.keycode) {
			case ALLEGRO_KEY_RIGHT:
				break;
			case ALLEGRO_KEY_LEFT:
				break;
			case ALLEGRO_KEY_DOWN:
				break;
			case ALLEGRO_KEY_UP:
				break;
			}
		}
	}
}

void MapScreen::render() {
	// Set up a transformation so we draw the map out and switch to where we need to draw
	ALLEGRO_TRANSFORM transform;
	al_identity_transform(&transform);
	//al_translate_transform(&transform, -dx, -dy);
	al_orthographic_transform(&transform, 0, 0, -1, Director::mode.width / 32, Director::mode.height / 32, 1);
	al_set_projection_transform(Director::display, &transform);

	// Draw image layers
	// TODO: Draw these

	// Draw normal tiles
	for (auto layer : layers) {
		if (layer->visible) {
			for (auto tile : layer->tiles) {
				// If tile is in bounds
				al_draw_scaled_rotated_bitmap(
					tile->tile,
					//tile->width, tile->height,
					0, 0,
					tile->loc_x, tile->loc_y,
					1.0/32, 1.0/32,
					tile->angle, tile->flags);

				//cout << tile->loc_x << " " << tile->loc_y << endl;
			}
		}
	}

	// NOTE: This is what's breaking
	al_draw_rectangle(0.0 + 1.0/64.0, 0.0 + 1.0/64.0, 1.0, 1.0, al_map_rgb(255, 0, 0), 0);
	al_draw_rectangle(1.0 + 1.0/64.0, 1.0 + 1.0/64.0, 2.0, 2.0, al_map_rgb(255, 0, 0), 0);

	// Draw object layers
	for (auto object_group : object_groups) {
		for (auto object : object_group->objects) {
			object->render();
		}
	}

	/*al_draw_rectangle(
		char_x * map.GetTileWidth() + tween_x + 1,
		char_y * map.GetTileHeight() + tween_y + 1,
		(char_x + 1) * map.GetTileWidth() + tween_x,
		(char_y + 1) * map.GetTileHeight() + tween_y,
		al_map_rgb(255, 255, 255),
		1);*/

	// Reset the transform just in case something else draws
	al_identity_transform(&transform);
	al_use_transform(&transform);
}
