#pragma once

#include "../src/screen.hpp"

#include <TmxParser/Tmx.h>
#include <allegro5/allegro_primitives.h>
#include <Box2D/Box2D.h>

#include <map>
#include <vector>
#include <set>

struct MapTile {
	ALLEGRO_BITMAP *tile = NULL;
	int flags = 0;
	float angle = 0;
	float width = 0;
	float height = 0;
	float loc_x = 0;
	float loc_y = 0;
};

struct MapLayer {
	std::vector<std::shared_ptr<MapTile>> tiles;
	bool visible;
	int height;
	int width;
};

class MapObject {
public:
	virtual void render() {}
};

class MapEllipse : public MapObject {
	float center_x;
	float center_y;
	float radius_x;
	float radius_y;

public:
	MapEllipse(Tmx::Object *object, b2World &world) {
		auto ellipse = object->GetEllipse();
		center_x = ellipse->GetCenterX();
		center_y = ellipse->GetCenterY();
		radius_x = ellipse->GetRadiusX();
		radius_y = ellipse->GetRadiusY();

		// NOTE: I don't want to support these as physics objects
	}

	void render() {
		al_draw_ellipse(
			center_x, center_y,
			radius_x, radius_y,
			al_map_rgb(255, 0, 0), 1.0f);
	}
};

class MapPolygon : public MapObject {
	float *points;
	int num_points;

public:
	MapPolygon(Tmx::Object *object, b2World &world) {
		auto polygon = object->GetPolygon();
		num_points = polygon->GetNumPoints();
		points = new float[num_points * 2];
		b2Vec2 *vertices = new b2Vec2[num_points];

		for (int i = 0; i < num_points; i++) {
			auto point = polygon->GetPoint(i);
			points[2*i] = (point.x + object->GetX()) / 32.0;
			points[2*i+1] = (point.y + object->GetY()) / 32.0;

			vertices[i].Set((point.x + object->GetX()) / 32.0f, (point.y + object->GetY()) / 32.0f);
		}

		// Make body
		b2BodyDef bodyDef;
		b2Body *body = world.CreateBody(&bodyDef);

		// Attach fixture
		b2PolygonShape poly;
		poly.Set(vertices, num_points);
		b2FixtureDef fixDef;
		fixDef.shape = &poly;

		body->CreateFixture(&fixDef);
		
		// Cleanup
		delete[] vertices;
	}

	~MapPolygon() {
		delete points;
	}

	void render() {
		al_draw_polygon(
			points, num_points,
			ALLEGRO_LINE_JOIN_BEVEL,
			al_map_rgb(255, 255, 0), 0.0f, 0.0f);
	}
};

class MapPolyline : public MapObject {
	float *points;
	int num_points;

public:
	MapPolyline(Tmx::Object *object, b2World &world) {
		auto polyline = object->GetPolyline();
		num_points = polyline->GetNumPoints();
		points = new float[num_points * 2];
		b2Vec2 *vertices = new b2Vec2[num_points];

		for (int i = 0; i < num_points; i++) {
			auto point = polyline->GetPoint(i);
			points[2*i] = (point.x + object->GetX()) / 32.0;
			points[2*i+1] = (point.y + object->GetY()) / 32.0;

			vertices[i].Set((point.x + object->GetX()) / 32.0f, (point.y + object->GetY()) / 32.0f);
		}

		// Make body
		b2BodyDef bodyDef;
		b2Body *body = world.CreateBody(&bodyDef);

		// Attach fixture
		b2ChainShape line;
		line.CreateChain(vertices, num_points);
		b2FixtureDef fixDef;
		fixDef.shape = &line;

		body->CreateFixture(&fixDef);
		
		// Cleanup
		delete[] vertices;
	}

	~MapPolyline() {
		delete points;
	}

	void render() {
		al_draw_polyline(
			points, sizeof(float) * 2, num_points,
			ALLEGRO_LINE_JOIN_BEVEL,
			ALLEGRO_LINE_CAP_NONE,
			al_map_rgb(0, 0, 255), 0.0f, 0.0f);
	}
};

class MapRectangle : public MapObject {
	float x1, y1, x2, y2;
	b2Body *body;

public:
	MapRectangle(Tmx::Object *object, b2World &world) {
		x1 = object->GetX() + 1;
		y1 = object->GetY() + 1;
		x2 = object->GetX() + object->GetWidth();
		y2 = object->GetY() + object->GetHeight();

		// Make body
		b2BodyDef bodyDef;
		body = world.CreateBody(&bodyDef);

		// Attach fixture
		b2PolygonShape box;
		float hw = object->GetWidth() / 2.0f;
		float hh = object->GetHeight() / 2.0f;
		box.SetAsBox(hw, hh, b2Vec2(object->GetX() + hw, object->GetY() + hh), 0);
		b2FixtureDef fixDef;
		fixDef.shape = &box;

		body->CreateFixture(&fixDef);
	}

	void render() {
		al_draw_rectangle(x1, y1, x2, y2, al_map_rgb(255, 255, 0), 1.0f);
	}
};

struct MapObjectGroup {
	std::vector<std::shared_ptr<MapObject>> objects;
};

class MapScreen : public Screen {
public:
	MapScreen(std::string filename);
	~MapScreen();
	void update(ALLEGRO_EVENT &ev);
	void render();

private:
	// Physics world
	b2World world;

	float timer = 0.0f;

	int tiles_to_draw_x = 0;
	int tiles_to_draw_y = 0;

	// Pressed keys
	std::set<int> pressed_keys;

	// The XML map
	Tmx::Map map;

	// Map info
	std::vector<std::shared_ptr<MapLayer>> layers;
	std::vector<std::shared_ptr<MapObjectGroup>> object_groups;

	// Tilesets
	std::vector<ALLEGRO_BITMAP *> tilesets;

	// Sub-Bitmaps, representing each separate tile
	std::map<int, ALLEGRO_BITMAP *> tiles;
};
