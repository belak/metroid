#pragma once

#include "../src/screen.hpp"

#include <allegro5/allegro_font.h>

#include <vector>

class MainMenuScreen : public Screen {
public:
	MainMenuScreen();
	~MainMenuScreen();
	void update(ALLEGRO_EVENT &ev);
	void render();

private:
	ALLEGRO_FONT *title_font = NULL;
	ALLEGRO_FONT *font = NULL;

	int selected_weight = 255;
	bool selected_shrinking = true;
	bool save_available = false;

	uint selected = 0;

	std::vector<std::string> items;
};
